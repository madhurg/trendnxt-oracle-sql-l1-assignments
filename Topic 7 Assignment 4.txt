SQL> EXPLAIN PLAN FOR select * from EMPLOYEE where MGR IS NULL;
SQL> SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY());

Output>

PLAN_TABLE_OUTPUT
Plan hash value: 1896031711

--------------------------------------------------------------------------
| Id | Operation | Name | Rows | Bytes | Cost (%CPU)| Time |
--------------------------------------------------------------------------
| 0 | SELECT STATEMENT | | 9 | 1197 | 3 (0)| 00:00:01 |
|* 1 | TABLE ACCESS FULL| EMPLOYEE | 9 | 1197 | 3 (0)| 00:00:01 |
--------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

1 - filter("MGR" IS NULL)

-----

After Indexing>

SQL> create index MGR_IDX ON Employee(upper(MGR));

SQL> EXPLAIN PLAN FOR select /*+ INDEX_RS_ASC(e MGR_IDX) */ * from EMPLOYEE e where MGR IS NULL;
SQL> SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY());

Output>

PLAN_TABLE_OUTPUT
Plan hash value: 4031004625

---------------------------------------------------------------------------------------
| Id | Operation | Name | Rows | Bytes | Cost (%CPU)| Time |
---------------------------------------------------------------------------------------
| 0 | SELECT STATEMENT | | 9 | 1197 | 2 (0)| 00:00:01 |
| 1 | TABLE ACCESS BY INDEX ROWID| EMPLOYEE | 9 | 1197 | 2 (0)| 00:00:01 |
|* 2 | INDEX RANGE SCAN | MGR_IDX | 9 | | 1 (0)| 00:00:01 |
---------------------------------------------------------------------------------------

