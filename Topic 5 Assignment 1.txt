a. insert into Employee values (100,'A','Raja',546,'10-JAN-19',2000,NULL,16);
   insert into Employee values (101,'B','Ram',546,'21-JAN-19',2000,NULL,16);
   insert into Employee values (102,'C','SAM',546,'09-JAN-19',2000,NULL,16);
   insert into Employee values (103,'D','ABC',546,'09-JAN-19',2000,5,16);
   insert into Employee values (104,'E','QWERT',546,'09-JAN-19',3000,NULL,16);
   insert into Employee values (105,'F','XYZ',546,'09-JAN-19',2000,NULL,16);
   insert into Employee values (106,'G','GSHS',579,'19-JAN-19',2000,NULL,16);
   insert into Employee values (107,'H','KRISH',579,'09-JAN-19',3000,NULL,16);
   insert into Employee values (108,'Z','CHRIS',549,'04-JAN-19',2000,10,16);
   insert into Employee values (109,'Y','GAYLE',549,'09-JAN-19',2000,NULL,16);
   insert into Employee values (110,'X','DHONI',549,'31-JAN-19',2000,NULL,16);
   insert into Employee values (120,'W','VIrat',579,'22-JAN-19',5000,NULL,16);
   insert into Employee values (130,'V','Sachin',200,'29-JAN-19',2000,NULL,16);
   insert into Employee values (140,'U','YUVRAJ',545,'31-JAN-19',2000,NULL,16);
   insert into Employee values (150,'T','Shewag',545,'09-JAN-19',7000,10,16);
b. update Employee SET SAL=SAL+(SAL*(10/100)) where DEPTNO in (10, 20);
c. UPDATE EMPLOYEE  SET SAL=SAL+(SAL*(10/100)),COMM=COMM+(COMM*(2/100)) WHERE COMM IS NOT NULL;

d. update Employee set JOB = 'MANAGER' where EMPNO in (select MGR from Employee);
e. delete from Employee where to_char(HIREDATE, 'YEAR') < 1940;
f. update Member set Max_Books_Allowed = 110 where Member_Id = 100;
   Error -> ORA-02290: check constraint CHK_MEMBER violated
   Reason -> Check constraint was set to Max_Books_Allowed column earlier where its value was set as must be greater than 100, hence the new value of 110 is violating the constraint.
g. insert into NewBook select * from Books;
h. COMMIT;
i. insert into Books values (1000, 'MongoDB', 'Allen', 750, 'Database');
   insert into Books values (1001, 'Python 3', 'Magret', 1350, 'AI');
   insert into Books values (1002, 'Angular 4', 'Google', 2750, 'UI');
   insert into Books values (1003, 'PL SQL-Ref', 'Scott Urman', 750, 'Database');
j. create type t_emp as object(EMPNO number, ENAME varchar2(40), JOB varchar2(20), MGR number, HIREDATE date, SAL number, COMM number, DEPTNO number);
   create type t_emplist as table of t_emp;
   insert into Employee values (5, t_emplist(t_emp(160,'I','WAITER',545,'18-MAR-95',5000,7,35),
											 t_emp(170,'J','WAITER',545,'18-MAR-95',5000,7,35),
											 t_emp(180,'K','WAITER',590,'18-MAR-95',8000,7,35),
											 t_emp(190,'L','WAITER',545,'18-MAR-95',5000,7,35),
											 t_emp(260,'M','WAITER',590,'18-MAR-95',2000,NULL));
k. ROLLBACK;
l. update Books set Cost = 300 and Category = 'OracleDatabase' where Book_No = 1003;
m. SAVEPOINT T;
   delete from Issue where Member_Id = 1 and Issue_Date < '10-DEC-06';
   delete from Books where Category not in (RDBMS', 'Database');
   ROLLBACK to T;
n. COMMIT;

